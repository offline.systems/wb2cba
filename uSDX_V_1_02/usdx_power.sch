EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:CAP C?
U 1 1 5FAC7BFE
P 4950 3850
AR Path="/5FAC7BFE" Ref="C?"  Part="1" 
AR Path="/5FA0DDC5/5FAC7BFE" Ref="C?"  Part="1" 
F 0 "C?" H 5128 3896 50  0000 L CNN
F 1 "100nF" H 5128 3805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4950 3850 50  0001 C CNN
F 3 "~" H 4950 3850 50  0001 C CNN
	1    4950 3850
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C?
U 1 1 5FAC7C04
P 6200 3850
AR Path="/5FAC7C04" Ref="C?"  Part="1" 
AR Path="/5FA0DDC5/5FAC7C04" Ref="C?"  Part="1" 
F 0 "C?" H 6378 3896 50  0000 L CNN
F 1 "100nF" H 6378 3805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 6200 3850 50  0001 C CNN
F 3 "~" H 6200 3850 50  0001 C CNN
	1    6200 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 5FAC7C0A
P 4500 3750
AR Path="/5FAC7C0A" Ref="C?"  Part="1" 
AR Path="/5FA0DDC5/5FAC7C0A" Ref="C?"  Part="1" 
F 0 "C?" H 4618 3796 50  0000 L CNN
F 1 "100u/16V" H 4350 3600 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4538 3600 50  0001 C CNN
F 3 "~" H 4500 3750 50  0001 C CNN
	1    4500 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3600 6200 3600
Wire Wire Line
	4500 3600 4950 3600
Wire Wire Line
	4950 3600 5250 3600
Connection ~ 4950 3600
Wire Wire Line
	4500 3900 4500 4100
Wire Wire Line
	4500 4100 4950 4100
Wire Wire Line
	4950 4100 5550 4100
Connection ~ 4950 4100
Wire Wire Line
	5550 3900 5550 4100
Connection ~ 5550 4100
Wire Wire Line
	5550 4100 6200 4100
Wire Wire Line
	5550 4100 5550 4300
Wire Wire Line
	3650 4100 4500 4100
Connection ~ 4500 4100
$Comp
L pspice:DIODE D?
U 1 1 5FAC7C1E
P 4050 3600
AR Path="/5FAC7C1E" Ref="D?"  Part="1" 
AR Path="/5FA0DDC5/5FAC7C1E" Ref="D?"  Part="1" 
F 0 "D?" H 4050 3865 50  0000 C CNN
F 1 "1N5819" H 4050 3774 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_AnodeUp" H 4050 3600 50  0001 C CNN
F 3 "~" H 4050 3600 50  0001 C CNN
	1    4050 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3600 4500 3600
Connection ~ 4500 3600
$Comp
L power:+12V #PWR?
U 1 1 5FAC7C26
P 4500 3450
AR Path="/5FAC7C26" Ref="#PWR?"  Part="1" 
AR Path="/5FA0DDC5/5FAC7C26" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4500 3300 50  0001 C CNN
F 1 "+12V" H 4515 3623 50  0000 C CNN
F 2 "" H 4500 3450 50  0001 C CNN
F 3 "" H 4500 3450 50  0001 C CNN
	1    4500 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3450 4500 3600
$Comp
L dk_Rectangular-Connectors-Headers-Male-Pins:22-23-2021 J?
U 1 1 5FAC7C36
P 3150 3600
AR Path="/5FAC7C36" Ref="J?"  Part="1" 
AR Path="/5FA0DDC5/5FAC7C36" Ref="J?"  Part="1" 
F 0 "J?" V 2925 3608 50  0000 C CNN
F 1 "DC 12V" V 3016 3608 50  0000 C CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm_Drill1.02mm" H 3350 3800 60  0001 L CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Molex%20PDFs/A-6373-N_Series_Dwg_2010-12-03.pdf" H 3350 3900 60  0001 L CNN
F 4 "WM4200-ND" H 3350 4000 60  0001 L CNN "Digi-Key_PN"
F 5 "22-23-2021" H 3350 4100 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 3350 4200 60  0001 L CNN "Category"
F 7 "Rectangular Connectors - Headers, Male Pins" H 3350 4300 60  0001 L CNN "Family"
F 8 "https://media.digikey.com/pdf/Data%20Sheets/Molex%20PDFs/A-6373-N_Series_Dwg_2010-12-03.pdf" H 3350 4400 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/molex/22-23-2021/WM4200-ND/26667" H 3350 4500 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN HEADER VERT 2POS 2.54MM" H 3350 4600 60  0001 L CNN "Description"
F 11 "Molex" H 3350 4700 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3350 4800 60  0001 L CNN "Status"
	1    3150 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 3600 3750 3600
Wire Wire Line
	3250 3700 3650 3700
Wire Wire Line
	3650 3700 3650 4100
$Comp
L dk_Barrel-Power-Connectors:PJ-102A J?
U 1 1 5FAC7C48
P 3550 3000
AR Path="/5FAC7C48" Ref="J?"  Part="1" 
AR Path="/5FA0DDC5/5FAC7C48" Ref="J?"  Part="1" 
F 0 "J?" H 3483 3225 50  0000 C CNN
F 1 "PJ-102A" H 3483 3134 50  0000 C CNN
F 2 "digikey-footprints:Barrel_Jack_5.5mmODx2.1mmID_PJ-102A" H 3750 3200 60  0001 L CNN
F 3 "https://www.cui.com/product/resource/digikeypdf/pj-102a.pdf" H 3750 3300 60  0001 L CNN
F 4 "CP-102A-ND" H 3750 3400 60  0001 L CNN "Digi-Key_PN"
F 5 "PJ-102A" H 3750 3500 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 3750 3600 60  0001 L CNN "Category"
F 7 "Barrel - Power Connectors" H 3750 3700 60  0001 L CNN "Family"
F 8 "https://www.cui.com/product/resource/digikeypdf/pj-102a.pdf" H 3750 3800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cui-inc/PJ-102A/CP-102A-ND/275425" H 3750 3900 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN PWR JACK 2X5.5MM SOLDER" H 3750 4000 60  0001 L CNN "Description"
F 11 "CUI Inc." H 3750 4100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3750 4200 60  0001 L CNN "Status"
	1    3550 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3000 3750 3000
Wire Wire Line
	3750 3000 3750 3600
Connection ~ 3750 3600
$Comp
L power:GNDPWR #PWR?
U 1 1 5FAC7C51
P 5550 4300
AR Path="/5FAC7C51" Ref="#PWR?"  Part="1" 
AR Path="/5FA0DDC5/5FAC7C51" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5550 4100 50  0001 C CNN
F 1 "GNDPWR" H 5554 4146 50  0000 C CNN
F 2 "" H 5550 4250 50  0001 C CNN
F 3 "" H 5550 4250 50  0001 C CNN
	1    5550 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3200 3650 3700
$Comp
L Regulator_Linear:LM1117-5.0 U?
U 1 1 5FAC7C58
P 5550 3600
AR Path="/5FAC7C58" Ref="U?"  Part="1" 
AR Path="/5FA0DDC5/5FAC7C58" Ref="U?"  Part="1" 
F 0 "U?" H 5550 3842 50  0000 C CNN
F 1 "LM1117-5.0" H 5550 3751 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 5550 3600 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm1117.pdf" H 5550 3600 50  0001 C CNN
	1    5550 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3600 3850 3600
$Comp
L Device:CP C?
U 1 1 5FACE3B8
P 6750 3750
F 0 "C?" H 6868 3796 50  0000 L CNN
F 1 "10uF" H 6868 3705 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 6788 3600 50  0001 C CNN
F 3 "~" H 6750 3750 50  0001 C CNN
	1    6750 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3600 6750 3600
Wire Wire Line
	6200 4100 6750 4100
Wire Wire Line
	6750 4100 6750 3900
Connection ~ 6750 3600
$Comp
L power:+5V #PWR?
U 1 1 5FACE3C3
P 6750 3350
F 0 "#PWR?" H 6750 3200 50  0001 C CNN
F 1 "+5V" H 6765 3523 50  0000 C CNN
F 2 "" H 6750 3350 50  0001 C CNN
F 3 "" H 6750 3350 50  0001 C CNN
	1    6750 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3350 6750 3600
Connection ~ 3650 3700
$EndSCHEMATC
