EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GNDPWR #PWR0101
U 1 1 5F236AE2
P 3900 3200
F 0 "#PWR0101" H 3900 3000 50  0001 C CNN
F 1 "GNDPWR" H 3904 3046 50  0000 C CNN
F 2 "" H 3900 3150 50  0001 C CNN
F 3 "" H 3900 3150 50  0001 C CNN
	1    3900 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5F243DB1
P 3900 3200
F 0 "TP1" H 3958 3318 50  0000 L CNN
F 1 "TestPoint" H 3958 3227 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D3.0mm" H 4100 3200 50  0001 C CNN
F 3 "~" H 4100 3200 50  0001 C CNN
	1    3900 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0102
U 1 1 5F245988
P 4750 3200
F 0 "#PWR0102" H 4750 3000 50  0001 C CNN
F 1 "GNDPWR" H 4754 3046 50  0000 C CNN
F 2 "" H 4750 3150 50  0001 C CNN
F 3 "" H 4750 3150 50  0001 C CNN
	1    4750 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5F24598E
P 4750 3200
F 0 "TP2" H 4808 3318 50  0000 L CNN
F 1 "TestPoint" H 4808 3227 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D3.0mm" H 4950 3200 50  0001 C CNN
F 3 "~" H 4950 3200 50  0001 C CNN
	1    4750 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0103
U 1 1 5F245EB4
P 5450 3200
F 0 "#PWR0103" H 5450 3000 50  0001 C CNN
F 1 "GNDPWR" H 5454 3046 50  0000 C CNN
F 2 "" H 5450 3150 50  0001 C CNN
F 3 "" H 5450 3150 50  0001 C CNN
	1    5450 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5F245EBA
P 5450 3200
F 0 "TP3" H 5508 3318 50  0000 L CNN
F 1 "TestPoint" H 5508 3227 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D3.0mm" H 5650 3200 50  0001 C CNN
F 3 "~" H 5650 3200 50  0001 C CNN
	1    5450 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0104
U 1 1 5F246464
P 6450 3200
F 0 "#PWR0104" H 6450 3000 50  0001 C CNN
F 1 "GNDPWR" H 6454 3046 50  0000 C CNN
F 2 "" H 6450 3150 50  0001 C CNN
F 3 "" H 6450 3150 50  0001 C CNN
	1    6450 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5F24646A
P 6450 3200
F 0 "TP4" H 6508 3318 50  0000 L CNN
F 1 "TestPoint" H 6508 3227 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D3.0mm" H 6650 3200 50  0001 C CNN
F 3 "~" H 6650 3200 50  0001 C CNN
	1    6450 3200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
